import Vue from 'vue';
import InputButton from '../packages/InputButton';

const components = {
  InputButton,
};

Object.keys(components).forEach((name) => {
  Vue.component(name, components[name]);
});

export default components;
