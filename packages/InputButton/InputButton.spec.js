import { mount } from '@vue/test-utils';
import InputButton from './InputButton.vue';

describe('InputButton component', () => {
  it('should render', () => {
    const component = mount(InputButton, {
      propsData: {
        variant: 'primary',
      },
      slots: {
        default: 'Button',
      },
    });
    expect(component.text()).toContain('Button');
  });

  it('should have class btn-<variant>', () => {
    const component = mount(InputButton, {
      propsData: {
        variant: 'secondary',
      },
      slots: {
        default: 'Button',
      },
    });
    expect(component.classes()).toContain('btn-secondary');
  });

  it('should emit click event when clicked', () => {
    const component = mount(InputButton, {
      propsData: {
        variant: 'primary',
      },
      slots: {
        default: 'Button',
      },
    });

    const element = component.find('button');
    element.trigger('click');

    expect(component.emitted('click')).toBeTruthy();
  });
});
