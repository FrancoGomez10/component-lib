/* eslint-disable import/no-extraneous-dependencies */
import { action } from '@storybook/addon-actions';
import { select, text, boolean } from '@storybook/addon-knobs';
import InputButton from './InputButton.vue';

export default {
  component: InputButton,
  title: 'Inputs|InputButton',
};

const variants = [
  'primary',
  'secondary',
  'success',
  'danger',
  'warning',
  'info',
  'light',
  'dark',
  'link',
];

const sizes = ['sm', 'md', 'lg'];

export const Button = () => ({
  components: { InputButton },
  template: `
    <div class="p-5">
      <InputButton
        :variant="variant"
        :size="size"
        :outlined="outlined"
        :rounded="rounded"
        :block="block"
        :disabled="disabled"
        :loading="loading"
        @click="clicked"
      >
        {{text}}
      </InputButton>
    </div>
  `,
  methods: {
    clicked: action('clicked'),
  },
  props: {
    variant: {
      type: String,
      default: select('variant', variants, 'primary'),
    },
    size: {
      type: String,
      default: select('size', sizes, 'md'),
    },
    text: {
      type: String,
      default: text('text', 'Button'),
    },
    disabled: {
      type: Boolean,
      default: boolean('disabled', false),
    },
    loading: {
      type: Boolean,
      default: boolean('loading', false),
    },
    block: {
      type: Boolean,
      default: boolean('block', false),
    },
    rounded: {
      type: Boolean,
      default: boolean('rounded', false),
    },
    outlined: {
      type: Boolean,
      default: boolean('outlined', false),
    },
  },
});
