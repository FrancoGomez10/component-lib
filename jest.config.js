module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  testMatch: ['**/packages/**/?(*.)+(spec|test).[jt]s'],
};
